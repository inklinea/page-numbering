#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##################################################################################
# inklinea library - Some functions to make extensions easier for - Inkscape 1.1+
##################################################################################

import inkex

# Only import standard libraries here
# Otherwise if a system with
import random
import time


class Inklin:

    #Unit conversions
    conversions = {
        'in': 96.0,
        'pt': 1.3333333333333333,
        'px': 1.0,
        'mm': 3.779527559055118,
        'cm': 37.79527559055118,
        'm': 3779.527559055118,
        'km': 3779527.559055118,
        'Q': 0.94488188976378,
        'pc': 16.0,
        'yd': 3456.0,
        'ft': 1152.0,
        '': 1.0,  # Default px
    }

    # Functions to silence stderr and stdout
    # Close output pipeline ( See notes at top of script )
    def set_stdout(self, state):
        import sys, os
        if state == 'off':
            sys.stdout = open(os.devnull, 'w')
        else:
            sys.stdout.close()
            sys.stdout = sys.__stdout__

    def set_stderr(self, state):
        import sys, os
        if state == 'off':
            sys.stderr = open(os.devnull, 'w')
        else:
            sys.stderr.close()
            sys.stderr = sys.__stderr__

    # Workaround for Inkscape 1.1+ shape/image units bug.
    def shape_unit_fix(self):
        """
        Inkscape 1.1 returns paths in user units but shapes and images in pixels. \n
        This is a user selectable conversions factor ( usually a checkbox ) \n
        which is stored at self.conversion_factor
        """
        found_units = self.svg.unit
        if self.options.shape_unit_fix == 'true':
            self.conversion_factor = Inklin.conversions[found_units]
        else:
            self.conversion_factor = 1

    def rgb_long_to_svg_rgb(self, rgb_long):
        from inkex import Color
        my_rgb = Color.parse_str(rgb_long)
        my_rgb_numbers = str(my_rgb[1]).replace('[', '').replace(']', '')
        svg_rgb = f'rgb({my_rgb_numbers})'
        return svg_rgb

    # Could not find simplestyle, found this instead in extensions repo
    def formatStyle(a):
        """Format an inline style attribute from a dictionary"""
        return ";".join([att + ":" + str(val) for att, val in a.items()])

    # Platform Check
    ################
    def os_check(self):
        """
        Check which OS we are using
        :return: OS Name ( windows, linux, macos )
        """
        from sys import platform

        if 'linux' in platform.lower():
            return 'linux'
        elif 'darwin' in platform.lower():
            return 'macos'
        elif 'win' in platform.lower():
            return 'windows'

    # Function to list object attributes
    ####################################

    def get_attributes(self):
        """ Returns a string containing all object attributes
             - One attribute per line
        """
        attribute_string = 'test'
        for att in dir(self):
            try:
                attribute = (att, getattr(self, att))
                attribute_string = attribute_string + str(attribute) + '\n'
            except:
                None
        return attribute_string

    # Extra path functions
    #######################

    # Return a rectangle path

    # Create Groups
    def create_new_group(self, parent, prefix, mode, suffix_type='epoch'):
        """
        Create a new group or Inkscape layer in document and return group

        :param parent: Parent group will be appended to
        :param prefix: The prefix for the group name
        :param mode: Inkscape 'layer' or 'group'
        :param suffix_type: The suffix for the group name
        :return: Group Object
        """

        from inkex import Group

        if suffix_type == 'random':
            id_suffix = str(random.randrange(1000000, 9999999))
        if suffix_type == 'epoch':
            id_suffix = str(time.time())

        group_id = str(prefix) + '_' + id_suffix
        new_group = parent.add(Group.new(group_id))
        new_group.set('inkscape:groupmode', str(mode))
        new_group.attrib['id'] = group_id

        return new_group

    # Colour functions
    ##################

    def random_rgb_string(self):
        """
        Return a random rgb colour string \n
        rgb(0-255, 0-255, 0-255)
        """
        random_red = random.randrange(0, 255)
        random_green = random.randrange(0, 255)
        random_blue = random.randrange(0, 255)

        return f'rgb({random_red}, {random_green}, {random_blue})'

    # Could not find simplestyle, found this instead in extensions repo
    def format_style(my_style):
        """Format an inline style attribute from a dictionary"""
        return ";".join([att + ":" + str(val) for att, val in my_style.items()])

    # Image functions
    #################

    def embedded_image_to_PIL(self, embedded_image):
        """
        If <image> is embedded, do base64string to PIL image
        """

        from PIL import Image
        from io import BytesIO
        import base64

        my_href = embedded_image.get('xlink:href')
        base64_string = my_href.split('base64,')[1]
        img_stream = BytesIO()
        img_stream.write(base64.b64decode(base64_string))
        im = Image.open(img_stream)
        return im

    def linked_image_to_pil(self, image_path):
        """
        If <image> is linked, open and return PIL image.
        """
        from PIL import Image
        im = Image.open(image_path)
        # inkex.errormsg((im.format, im.size, im.mode))
        im.close()
        return im

    def image_to_pil(self, image_element):
        """
        Return a PIL image from an <image> ( linked or embedded )
        """
        if image_element.TAG.lower() == 'image':
            # base64 in string indicated embedded image
            if 'base64' in image_element.tostring().decode('utf-8').lower():
                im = Inklin.embedded_image_to_PIL(self, image_element)
                return im

            # Otherwise it will be a linked image
            else:
                try:
                    image_path = image_element.get('xlink:href')
                    image_real_path = image_path.split('file://')[1]
                    im = Inklin.linked_image_to_pil(self, image_real_path)
                    return im
                except:
                    return None

    def pil_to_base64_image_string(self, im):
        """
        Return a png base64 string from a PIL image
        :param im: PIL image
        :return: Base64 encoded png image
        """
        from PIL import Image
        from io import BytesIO
        import base64

        img_stream = BytesIO()
        im.save(img_stream, format='PNG')
        byte_img = img_stream.getvalue()
        base64_img_str = base64.b64encode(byte_img).decode('utf-8')

        return base64_img_str

    def pil_to_base64_image_object(self, parent, base_id, im):
        """
        Create an <image> object with base64 string (embedded) png image on canvas \n
        :param base_id: The id of the created object
        :param im: The PIL source image
        """
        from lxml import etree
        # Inscape 1.1
        conversion_factor = self.conversion_factor

        my_image = etree.SubElement(self.svg, inkex.addNS('image', 'svg'))

        conversion_factor = self.conversion_factor

        base64_img_str = Inklin.pil_to_base64_image_string(self, im)

        # Lets place the preview image off the canvas
        my_image.attrib['x'] = str(0)
        my_image.attrib['y'] = str((-(im.size[1]) / conversion_factor))

        my_image.attrib['width'] = str(im.size[0] / conversion_factor)
        my_image.attrib['height'] = str(im.size[1] / conversion_factor)

        my_image.attrib['id'] = f'{base_id}'

        my_image.set('xlink:href', str(f'data:image/png;base64,{base64_img_str}'))




    # Command Line
    ##############

    # Return a svg file copy in a temp folder
    # This is due to permission problems with tempfile in Windows

    def make_temp_folder(self):
        """
        Creates a temp folder to which files can be written \n
        To remove folder at end of script use: \n
        # Cleanup temp folder \n
        if hasattr(self, 'inklin_temp_folder'):
            shutil.rmtree(self.inklin_temp_folder)

        :return: A temp folder path string
        """
        import tempfile
        temp_folder = tempfile.mkdtemp()
        self.inklin_temp_folder = temp_folder
        return temp_folder

    def make_temp_file_copy(self, my_file, extension='.svg'):
        import shutil
        import os
        import time
        temp_file_name = str(time.time()).replace('.', '') + extension
        if hasattr(self, 'inklin_temp_folder'):
            temp_folder = self.inklin_temp_folder
        else:
            temp_folder = Inklin.make_temp_folder(self)
        temp_file = shutil.copy(my_file, os.path.join(temp_folder, temp_file_name))
        temp_file_object = open(temp_file)
        temp_file_object.temp_folder = temp_folder
        return temp_file_object

    def inkscape_command_call(self, input_file, options_list, action_list):
        """
        A function to execute an Inkscape command call on a temp_file copy of the
        input_file and return the resulting file object. \n
       :param input_file: input file path ( usually self.options.input_file )
       :param options_list: inkscape command line options
       :param action_list: inkscape command line actions ( and verbs in Inskcape 1.1 )
       :return: Returns the resulting svg file object ( which must be .closed() later )
       """
        from inkex import command
        # First make a copy of the input_file
        temp_svg = Inklin.make_temp_file_copy(self, input_file)
        # Then run the command line
        command.inkscape(temp_svg.name, options_list, f'--actions={action_list}')
        return temp_svg

    def inkscape_command_call_stdout(self, input_file, options_list, action_list):
        """
        A function to execute an Inkscape command call on a temp_file copy of the
        input_file and return stdout only. Useful for select-list etc.\n
        :param input_file: input file path ( usually self.options.input_file )
        :param options_list: inkscape command line options
        :param action_list: inkscape command line actions ( and verbs in Inskcape 1.1 )
        :return: Returns the stdout from the command line call
        """
        from inkex import command
        # First make a copy of the input_file
        temp_svg = Inklin.make_temp_file_copy(self, input_file)
        # Then run the command line
        stdout = command.inkscape(temp_svg.name, options_list, f'--actions={action_list}')
        temp_svg.close()
        return stdout

    # query-all bbox object which can be appended to end of bbox dictionary
    class QueryAllBbox():
        x1 = y1 = x2 = y2 = x3 = y3 = x4 = y4 = mid_x = mid_y = width = height = 0
        offset_x = offset_y = 0

    def inkscape_command_call_bboxes_to_dict(self, input_file):
        """
        A function to return a dictionary of all element bounding boxes
        -- this function is visual ( includes stroke etc ) rather than just
        geometric ( just path bbox ) \n
        :param input_file: input file path ( usually self.options.input_file )
        :return: Returns the results of --query-all in a dictionary
        -- last entry in dictionary is also a VisualBbox object
        """
        from inkex import command
        # First make a copy of the input_file
        temp_svg = Inklin.make_temp_file_copy(self, input_file)
        # Then run the command line
        my_query = command.inkscape(temp_svg.name, '--query-all')

        # Account for versions of inkey.py which return query as bytes
        if type(my_query) != str:
            my_query = my_query.decode("utf-8")
        # --query-all produces multiline output of the following format
        # path853,172.491,468.905,192.11,166.525 - as string
        # ElementId, Top, Left, Width, Height

        # Make a list splitting by each new line
        my_query_items = my_query.split('\n')
        my_element_bbox_dict = {}

        for my_query_item in my_query_items:
            # Create a comma separated list item for each line
            my_element = my_query_item.split(',')
            # Make a dictionary for all elements, rejected malformed elements.
            if len(my_element) > 4:
                my_element_bbox_dict[my_element[0]] = {}
                # Create Dictionary entry in anticlockwise format
                # x1 = TopLeft, x2 = BottomLeft, x3 = BottomRight, x4 = TopRight, mid_x and mid_y

                # First convert all values to float, skipping element id ( first entry )
                my_element_bbox = [float(x) for x in my_element[1:]]

                width = my_element_bbox[2]
                height = my_element_bbox[3]

                x1 = my_element_bbox[0]
                y1 = my_element_bbox[1]
                x2 = x1
                y2 = y1 + height
                x3 = x1 + width
                y3 = y2
                x4 = x1 + width
                y4 = y1
                mid_x = x1 + width / 2
                mid_y = y1 + height / 2

                # Let's attach an object at the end of the dictionary entry

                query_all_bbox = Inklin.QueryAllBbox()
                query_all_bbox.x1 = x1
                query_all_bbox.y1 = y1
                query_all_bbox.x2 = x2
                query_all_bbox.y2 = y2
                query_all_bbox.x3 = x3
                query_all_bbox.y3 = y3
                query_all_bbox.x4 = x4
                query_all_bbox.y4 = y4
                query_all_bbox.mid_x = mid_x
                query_all_bbox.mid_y = mid_y
                query_all_bbox.width = width
                query_all_bbox.height = height

                my_element_bbox_dict[my_element[0]].update(x1=x1, y1=y1, x2=x2, y2=y2, x3=x3, y3=y3, x4=x4, y4=y4,
                                                           mid_x=mid_x, mid_y=mid_y, width=width, height=height, query_all_bbox=query_all_bbox)
        # Return dictionary
        temp_svg.close()
        return my_element_bbox_dict

    def get_text_string_offset(self, temp_folder, bbox_type, string_list, font_dict):
        from inkex import SvgDocumentElement, Group, TextElement, Tspan
        import os, uuid
        from copy import deepcopy
        # By setting text x and y to zero, we can find the bbox offset from zero.
        # This is important as x and y sets position of text-anchor

        text_svg = deepcopy(self.svg)

        text_group = Group()
        string_index = 1

        for string in string_list:
            text_element = TextElement()
            tspan_element = Tspan()
            tspan_element.text = string
            text_element.style['fill'] = font_dict['font_color']
            true_font_size = font_dict['font_size'] / self.cf * Inklin.conversions[font_dict['font_units']]
            text_element.style['font-size'] = str(true_font_size)
            text_element.style['font-family'] = font_dict['font_family']
            text_element.style['font-style'] = font_dict['font_style']
            text_element.style['font-weight'] = font_dict['font_weight']

            text_element.set('id', f'text{string_index}')
            text_element.set('x', '0')
            text_element.set('y', '0')
            text_element.append(tspan_element)
            text_group.append(text_element)
            group_id = str(uuid.uuid4())
            text_group.set('id', group_id)
            string_index += 1

        text_svg.append(text_group)

        query_temp_filename = str(uuid.uuid4()) + '.svg'
        query_temp_filepath = os.path.join(temp_folder, query_temp_filename)

        # Strip all styles for geometric bbox_type
        if bbox_type == 'geometric':
            query_svg = deepcopy(text_svg)
            text_group_id = text_group.get_id()

            query_text_group = query_svg.getElementById(text_group_id)

            skip_elements = query_text_group.getchildren()
            Inklin.remove_all_styles(self, query_svg, skip_elements, styles=True, style_elements=True, classes=True, attributes=True)
        else:
            query_svg = text_svg

        with open(query_temp_filepath, 'w') as query_temp_file:
            my_svg_string = query_svg.root.tostring().decode("utf-8")
            query_temp_file.write(my_svg_string)
            query_temp_file.close()
            new_bbox_dict = Inklin.inkscape_command_call_bboxes_to_dict(self, query_temp_filepath)

        Inklin.attach_bbox_to_object(self, text_group.getchildren(), new_bbox_dict)
        return new_bbox_dict, text_group, text_svg


    def attach_bbox_to_object(self, object_list, bbox_dict):
        for object_item in object_list:
            object_id = object_item.get('id')
            if object_id in bbox_dict.keys():
                object_item.query_all_bbox = bbox_dict[object_id]['query_all_bbox']

    def remove_all_styles(self, svg_element, skip_elements, styles=False, style_elements=False, classes=False, attributes=False):
        # Remove all styles / classes / fill and stroke attributes
        all_elements = svg_element.xpath('//*')

        if style_elements:
            style_elements = svg_element.xpath('//svg:style')
            for style_element in style_elements:
                style_element.delete()

        # inkex.errormsg(all_elements)
        for element in all_elements:
            if element not in skip_elements:
                if styles:
                    element.style = {}

            # if style_elements:
            #     style_elements = element.xpath('//svg:style')
            #     for style_element in style_elements:
            #         style_element.delete()

            if classes:
                if element not in skip_elements:
                    if 'class' in element.attrib.keys():
                        element.attrib.pop('class')

            if attributes:
                if element not in skip_elements:
                    if 'stroke-width' in element.attrib.keys():
                        element.attrib.pop('stroke-width')
                    if 'stroke' in element.attrib.keys():
                        element.attrib.pop('stroke')
                    if 'fill' in element.attrib.keys():
                        element.attrib.pop('fill')

        return element

    def csv_to_string_list(self, csv_filepath):
        import csv
        csv_string_list = []

        try:
            with open(csv_filepath, newline='') as csvfile:

                my_csv_data = csv.reader(csvfile, delimiter=',', quotechar='"')

                for row in my_csv_data:
                    csv_string_list.append(row)
        except:
            inkex.errormsg('Cannot Open CSV File')
            return

        return csv_string_list

    def check_filepath_writable(self, folderpath, filename):
        import os

        filepath = os.path.join(folderpath, filename)

        try:
            try_file = open(filepath, 'w')
            try_file.close()
            os.remove(filepath)
            return True
        except:
            # inkex.errormsg('filepath is not valid ( or non writeable ) ')
            return False

    def check_output_filepath(self, export_folder, base_filename):
        import os, sys
        # Test Export Folder writable with known good filename
        test_filename = (str(time.time())).replace('.', '')
        if Inklin.check_filepath_writable(self, export_folder, test_filename):
            # inkex.errormsg('Export Folder is Writeable')
            None
        else:
            inkex.errormsg('Export Folder is *Not Writeable*')
            sys.exit()
        # Test Export Filename
        if Inklin.check_filepath_writable(self, export_folder, base_filename):
            # inkex.errormsg('Export Folder is okay')
            export_filepath = os.path.join(export_folder, base_filename)
            return export_filepath
        else:
            inkex.errormsg('Base Filename is *invalid*')
            sys.exit()