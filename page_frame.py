import random

import inkex
from inkex import Rectangle, Layer

from inklinea import Inklin

import datetime

def get_page_bbox_dict(self, page):

    page_bbox = {}
    page_bbox['left'] = float(page.get('x'))
    page_bbox['top'] = float(page.get('y'))
    page_bbox['width'] = float(page.get('width'))
    page_bbox['height'] = float(page.get('height'))

    return page_bbox


def frame_pages(self, svg, margin_dict, style_dict, page_frame_layer, cf):

    page_list = svg.xpath('//inkscape:page')

    if len(page_list) < 1:
        page_bbox = self.svg.get_page_bbox()
        page_bbox_dict = {}
        page_bbox_dict['left'] = page_bbox.left
        page_bbox_dict['top'] = page_bbox.top
        page_bbox_dict['width'] = page_bbox.width
        page_bbox_dict['height'] = page_bbox.height
        make_page_frame(self, self.svg, page_bbox_dict, margin_dict, style_dict, page_frame_layer, 1, cf)
    else:
        for page_count, page in enumerate(page_list):

            page_bbox = get_page_bbox_dict(self, page)
            # page_bbox['left'] = float(page.get('x'))
            # page_bbox['top'] = float(page.get('y'))
            # page_bbox['width'] = float(page.get('width'))
            # page_bbox['height'] = float(page.get('height'))

            make_page_frame(self, self.svg, page_bbox, margin_dict, style_dict, page_frame_layer, page_count, cf)

    descendants = self.svg.descendants()
    descendants[1].addnext(page_frame_layer)

    return page_frame_layer

def make_page_frame(self, svg, page_bbox, margin_dict, style_dict, parent, page_count, cf):

    x = page_bbox['left'] + (margin_dict['left']) * cf
    y = page_bbox['top'] + (margin_dict['top']) * cf
    width = page_bbox['width'] - ((margin_dict['left'] + margin_dict['right'])) * cf
    height = page_bbox['height'] - ((margin_dict['top'] + margin_dict['bottom'])) * cf

    # Work around if user chooses margins
    # Which are larger than the page area
    width = abs(width)
    height = abs(height)

    # random id
    rand_id = random.randrange(100000, 2000000)

    margin_rect = Rectangle()
    margin_rect_label = f'margin_rect_{page_count + 1}'
    margin_rect.set('inkscape:label', margin_rect_label)
    margin_rect.set('id', rand_id)
    margin_rect.set('x', x)
    margin_rect.set('y', y)
    margin_rect.set('width', width)
    margin_rect.set('height', height)
    margin_rect.style['fill'] = style_dict['fill']
    margin_rect.style['stroke'] = style_dict['stroke']
    margin_rect.style['stroke-width'] = style_dict['stroke-width']

    parent.append(margin_rect)

    # inkex.errormsg(f'Width: {width} Height: {height} Id: {rand_id}')

    return parent


class PageFrame(inkex.EffectExtension):

    def add_arguments(self, pars):

        # Margins
        pars.add_argument("--top_margin", type=float, dest="top_margin", default=16)
        pars.add_argument("--left_margin", type=float, dest="left_margin", default=16)
        pars.add_argument("--bottom_margin", type=float, dest="bottom_margin", default=16)
        pars.add_argument("--right_margin", type=float, dest="right_margin", default=16)
        pars.add_argument("--margin_units_combo", type=str, dest="margin_units_combo", default='px')
        pars.add_argument("--stroke_cp", type=str, dest="stroke_cp", default='255')
        pars.add_argument("--fill_cp", type=str, dest="fill_cp", default='4294967295')
        pars.add_argument("--stroke_width", type=float, dest="stroke_width", default='0')

    def effect(self):

        # inkex.errormsg(Inklin.get_attributes(self.svg))

        self.units = self.svg.unit

        chosen_units = self.options.margin_units_combo

        frame_cf = Inklin.conversions[chosen_units] / Inklin.conversions[self.units]

        page_list = self.svg.xpath('//inkscape:page')

        margin_dict = {}
        margin_dict['top'] = self.options.top_margin
        margin_dict['left'] = self.options.left_margin
        margin_dict['bottom'] = self.options.bottom_margin
        margin_dict['right'] = self.options.right_margin

        style_dict = {}
        style_dict['stroke'] = Inklin.rgb_long_to_svg_rgb(self, self.options.stroke_cp)
        style_dict['stroke-width'] = self.options.stroke_width
        style_dict['fill'] = Inklin.rgb_long_to_svg_rgb(self, self.options.fill_cp)

        page_frame_layer = Layer()
        page_frame_layer_id = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
        page_frame_layer.set('id', f'page_frame_layer_{page_frame_layer_id}')

        # If there is only one page
        if len(page_list) < 1:
            page_bbox = self.svg.get_page_bbox()
            page_bbox_dict = {}
            page_bbox_dict['left'] = float(page_bbox.left)
            page_bbox_dict['top'] = float(page_bbox.top)
            page_bbox_dict['width'] = float(page_bbox.width)
            page_bbox_dict['height'] = float(page_bbox.height)
            make_page_frame(self, self.svg, page_bbox_dict, margin_dict, style_dict, page_frame_layer, 1, frame_cf)
        else:
            for page_count, page in enumerate(page_list):
                page_bbox_dict = get_page_bbox_dict(self, page)
                make_page_frame(self, self.svg, page_bbox_dict, margin_dict, style_dict, page_frame_layer, page_count, frame_cf)

        descendants = self.svg.descendants()
        descendants[1].addnext(page_frame_layer)


if __name__ == '__main__':
    PageFrame().run()
