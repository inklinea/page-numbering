#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Font Chooser - Simple Gtk3 FontChooser to .ini file
##############################################################################

import inkex
import configparser
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from gi.repository import Gtk, GdkPixbuf, Gdk
import configparser
from inklinea import Inklin

def write_config_file(pango_font_desc_dict):

    font_chooser_config = configparser.ConfigParser()
    font_chooser_config['FONT'] = {}
    for key in pango_font_desc_dict.keys():
        font_chooser_config['FONT'][key] = str(pango_font_desc_dict[key])

    with open('font_chooser.ini', 'w') as configfile:
        font_chooser_config.write(configfile)


class Handler:

    def onDestroy(self, *args):
        Gtk.main_quit()
        pango_font_desc_dict = Handler.gtkFontChooser(self, scale=0)
        write_config_file(pango_font_desc_dict)

    def gtkFontChooser(self, scale):
        pango_font_desc = FontChooser.builder.get_object('gtk_font_chooser').get_font_desc()
        pango_font_desc_dict = {}
        pango_font_desc_dict['family'] = pango_font_desc.get_family()
        pango_font_desc_dict['style'] = pango_font_desc.get_style().value_nick
        pango_font_desc_dict['variant'] = pango_font_desc.get_variant().value_nick
        pango_font_desc_dict['weight'] = pango_font_desc.get_weight().value_nick
        pango_font_desc_dict['size'] = pango_font_desc.get_size()
        return pango_font_desc_dict

def run_gtk():
    # Build interface from .glade file
    FontChooser.builder = Gtk.Builder()
    FontChooser.builder.add_from_file("font_chooser.glade")
    FontChooser.builder.connect_signals(Handler())

    FontChooser.window = FontChooser.builder.get_object("main_window")
    FontChooser.window.show_all()
    FontChooser.window.set_title('Font Chooser')

    # End of Gtk main loop
    Gtk.main()


class FontChooser(inkex.EffectExtension):

    def effect(self):
        run_gtk()


if __name__ == '__main__':
    FontChooser().run()
