import inkex

from inklinea import Inklin

def create_labels(self, temp_folder, string_list, selection_list, font_dict, alignment_dict, parent):
    import time
    """
    Generic Font Families are:
    'serif' (e.g., Times)
    'sans-serif' (e.g., Helvetica)
    'cursive' (e.g., Zapf-Chancery)
    'fantasy' (e.g., Western)
    'monospace' (e.g., Courier)
    """
    bbox_type = self.options.bbox_type_radio

    # text_svg is a returned deepcopy of the svg
    string_list_bbox, returned_text_group, text_svg = Inklin.get_text_string_offset(self, temp_folder, bbox_type,
                                                                                    string_list, font_dict)

    # inkex.errormsg(string_list_bbox)

    if parent == 'label_svg':
        parent = text_svg

    # Create new group to hold labels:
    label_group = Inklin.create_new_group(self, parent, 'labels', 'layer', 'epoch')

    label_index = 1

    for text_element, selected in zip(returned_text_group, selection_list):

        text_element.query_all_bbox = string_list_bbox[text_element.get_id()]['query_all_bbox']
        text_offset_x = text_element.query_all_bbox.x1 * -1
        text_offset_y1 = text_element.query_all_bbox.y1 * -1
        text_offset_y2 = text_element.query_all_bbox.y2 * -1
        text_offset_ydiff = text_offset_y2 - text_offset_y1

        selected_width = string_list_bbox[selected.get_id()]['width']
        selected_height = string_list_bbox[selected.get_id()]['height']
        text_width = text_element.query_all_bbox.width
        text_height = text_element.query_all_bbox.height
        x_difference = selected_width - text_width
        y_difference = selected_height - text_height
        selected_y = string_list_bbox[selected.get_id()]['y1']
        selected_x = string_list_bbox[selected.get_id()]['x1']

        if alignment_dict['h_align'] == 'left':
            text_element_x = selected_x + text_offset_x
        if alignment_dict['h_align'] == 'center':
            text_element_x = selected_x + (x_difference / 2) + text_offset_x
        if alignment_dict['h_align'] == 'right':
            text_element_x = selected_x + x_difference + text_offset_x

        if alignment_dict['v_align'] == 'top':
            text_element_y = selected_y
        if alignment_dict['v_align'] == 'middle':
            text_element_y = selected_y + (y_difference / 2) + text_offset_y1
        if alignment_dict['v_align'] == 'bottom':
            text_element_y = selected_y + selected_height

        if alignment_dict['h_align'] != 'center':

            if alignment_dict['h_io'] == 'inside':
                if alignment_dict['h_align'] == 'left':
                    text_element_x = text_element_x
                if alignment_dict['h_align'] == 'right':
                    text_element_x = text_element_x

            if alignment_dict['h_io'] == 'outside':
                if alignment_dict['h_align'] == 'left':
                    text_element_x = text_element_x - text_width
                if alignment_dict['h_align'] == 'right':
                    text_element_x = text_element_x + text_width

        if alignment_dict['v_align'] != 'middle':

            if alignment_dict['v_io'] == 'inside':
                if alignment_dict['v_align'] == 'top':
                    text_element_y = text_element_y + text_height + text_offset_y2

                if alignment_dict['v_align'] == 'bottom':
                    text_element_y = text_element_y + text_height + text_offset_ydiff
                    if alignment_dict['perch'] == 'baseline':
                        text_element_y = text_element_y + text_offset_y2

            if alignment_dict['v_io'] == 'outside':
                if alignment_dict['v_align'] == 'top':
                    text_element_y = text_element_y + text_height + text_offset_ydiff
                    if alignment_dict['perch'] != 'baseline':
                        text_element_y = text_element_y + text_offset_y2
                if alignment_dict['v_align'] == 'bottom':
                    text_element_y = text_element_y + text_height + text_offset_y1 + text_offset_ydiff

        text_element.set('x', str(text_element_x / self.cf))
        text_element.set('y', str(text_element_y / self.cf))
        # Remove the text element id to force Inkscape to regenerate id
        # This prevents collisions on 2nd run
        # text_element.set('id', '')
        text_element.set('inkscape:label', f'label_{label_index}')
        text_element_duplicate = text_element.duplicate()
        text_element_duplicate.set('id', text_element.get_id())
        import uuid
        # text_element_duplicate.set('id', uuid.uuid4())
        # text_element_duplicate.set_random_id()
        label_group.append(text_element_duplicate)
        label_index += 1

    parent.append(label_group)
    # returned_text_group.delete()

    return text_svg, string_list_bbox, label_group
