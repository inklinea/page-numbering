#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Page Numbering - Some simple Page Numbering
#  An Inkscape 1.2+ extension
##############################################################################

import inkex

from inkex import Metadata, PathElement, Transform, Circle


from inklinea import Inklin

from create_labels import *
from page_watermark import *
from page_frame import *

import configparser
from copy import copy, deepcopy
from lxml import etree
import sys
import csv


def read_config_file(self, config_filename, section_name):
    try:
        config = configparser.ConfigParser()
        config.read(config_filename)
        config_dict = dict(config.items(section_name))
        return config_dict
    except:
        return None


def get_svg_no_children(self, svg):
    # Shallow copy just <svg ....> </svg>
    svg_shallow_copy = copy(svg)

    for item in svg_shallow_copy.getchildren():
        item.delete()

    svg_text = svg_shallow_copy.tostring().decode('utf-8')
    inkex.errormsg(svg_text)


# Returns a pruned tree for a given object
# all children on the tree are removed
# apart from the group / layer chain and terminal object
def get_pruned_object_tree(self, my_object):
    object_tree_list = [my_object]
    current_object = my_object

    while current_object.getparent().TAG != 'svg':
        current_parent = current_object.getparent()
        object_tree_list.insert(0, current_parent)
        current_object = current_parent

    # shallow copy each item
    copied_object_list = [copy(item) for item in object_tree_list]
    pruned_object_list = [xslt_prune_object(self, item) for item in copied_object_list]

    pruned_object_tree_root = pruned_object_list[0]

    for child in pruned_object_tree_root:
        child.delete()

    current_parent = pruned_object_list[0]
    for element in pruned_object_list[1:]:
        current_parent.append(element)
        current_parent = element

    inkex.errormsg(pruned_object_tree_root.tostring().decode('utf-8'))


# This is an xslt transform to just get the object alone
# and preserving their attributes.
def xslt_prune_object(self, my_object):
    from io import StringIO

    prune_transform = StringIO('''\
    <xsl:stylesheet version="1.0"
     xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
     <xsl:template match="*">
         <xsl:copy select=".">
           <xsl:copy-of select="@*"/>
        </xsl:copy>
        </xsl:template>
        </xsl:stylesheet>''')

    xslt_doc = etree.parse(prune_transform)
    transform = etree.XSLT(xslt_doc)

    result_tree = transform(my_object)

    return result_tree.getroot()


def strip_root_svg(self, my_object):
    my_object_copy = copy(my_object)

    svg_children = my_object_copy.xpath(
        './svg:circle | ./svg:ellipse | ./svg:line | ./svg:path | ./svg:text | ./svg:polygon | ./svg:polyline '
        '| ./svg:rect | ./svg:g | ./svg:use')

    for child in svg_children:
        child.delete()

    stripped_svg = my_object_copy

    return stripped_svg


# Need to refactor this !

def count_roman(self, num):
    increments = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
    numberals = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"]
    roman_numeral = ''
    count = 0
    while num > 0:
        for counter in range(num // increments[count]):
            roman_numeral += numberals[count]
            num -= increments[count]
        count += 1
    return roman_numeral

def create_dummy_marker(self, marker_url, line_style):
    marker_path = PathElement()
    marker_path.set('d', 'M 0 0 L 0.01 0')
    # marker_path.style['stroke'] = 'black'
    # marker_path.style['stroke-width'] = '1'
    marker_path.set('id', 'dummy_marker')
    # marker_path.style['marker-end'] = marker_url
    marker_path.style = line_style
    self.svg.append(marker_path)
    return marker_path

def get_marker(self, line_style):
    selection_list = self.svg.selected
    # See if we can grab the marker from first selected object
    if len(selection_list) > 0:
        marker_url = selection_list[0].style.get('marker-end')
        if marker_url is None:
            marker_url = selection_list[0].style.get('marker-start')
            if marker_url is None:
                marker_url = selection_list[0].style.get('marker-mid')
    else:
        marker_path = marker_url = None
    # Lets make a tiny line with a marker to get the bounding box

    if marker_url is not None:
        marker_path = create_dummy_marker(self, marker_url, line_style)
    else:
        marker_path = None

    return marker_path, marker_url

def create_label_lines(self, string_bbox_list, page_frame_layer, label_group, alignment_dict, margin_dict, marker_url, line_style):

    for frame, label in zip(page_frame_layer, label_group):
        frame_id = frame.get_id()
        label_id = label.get_id()

        frame_width = string_bbox_list[frame_id]['width'] / self.cf
        frame_left = string_bbox_list[frame_id]['x1'] / self.cf
        frame_top = string_bbox_list[frame_id]['y1'] / self.cf
        frame_bottom = string_bbox_list[frame_id]['y2'] / self.cf
        frame_mid_x = string_bbox_list[frame_id]['mid_x'] / self.cf
        frame_mid_y = string_bbox_list[frame_id]['mid_y'] / self.cf
        label_width = string_bbox_list[label_id]['width'] / self.cf
        label_height = string_bbox_list[label_id]['height'] / self.cf

        line_margin = self.options.simple_line_margin_float / self.cf

        line_a_length = (frame_width - label_width) / 2

        v_align = alignment_dict['v_align']

        if v_align == 'top':
            line_y = frame_top + label_height / 2
        elif v_align == 'middle':
            line_y = frame_mid_y
        elif v_align == 'bottom':
            line_y = frame_bottom - label_height / 2


        if marker_url is not None:
            marker_bbox = string_bbox_list['dummy_marker']
            # inkex.errormsg(marker_bbox)
            # Dummy Marker should be at 0,0
            # Therefore x1 should be left line outcrop
            # x3 should be line right outcrop
            # We should be able to use x3 - to make it touch label perfectly.
            marker_right_outcrop = marker_bbox['x3'] / self.cf
            # inkex.errormsg(marker_right_outcrop)
            marker_width = marker_bbox['width'] / self.cf
            marker_height = marker_bbox['height'] / self.cf
        else:
            marker_width = 0
            marker_height = 0

        line_a_path = PathElement()
        line_b_path = PathElement()

        if marker_url is not None:
            line_a_path.style['marker-end'] = marker_url
            line_a_path_d = f"M {frame_left} {line_y} L {frame_left + line_a_length - line_margin - marker_right_outcrop} {line_y}"

            line_b_path_d = f"M {frame_left + frame_width} {line_y}  L {frame_left + label_width + line_a_length + marker_right_outcrop} {line_y}"
            marker_id = marker_url.split('#')[1].replace(')', '')
            marker_b = self.svg.getElementById(marker_id).duplicate()
            line_b_path.style['marker-end'] = marker_b
        else:
            line_a_path_d = f"M {frame_left} {line_y} L {frame_left + line_a_length - line_margin} {line_y}"
            line_b_path_d = f"M {frame_left + frame_width} {line_y}  L {frame_left + label_width + line_a_length + line_margin} {line_y}"


        line_a_path.set('d', line_a_path_d)
        line_a_path.style = line_style
        self.svg.append(line_a_path)

        line_b_path.set('d', line_b_path_d)
        line_b_path.style = line_style
        self.svg.append(line_b_path)


# Creates a number suffixed char_set list
def create_char_set_number_list(self, list_length, numbering_type, numbering_preset='numbers'):
    label_list = []

    char_set = 'abcdefghijklmnopqrstuvwxyz'

    if numbering_type == 'preset':

        if numbering_preset == 'numbers':
            char_set = range(1, list_length)
        elif numbering_preset == 'lowercase':
            char_set = 'abcdefghijklmnopqrstuvwxyz'
        elif numbering_preset == 'uppercase':
            char_set = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        elif numbering_preset == 'greek':
            char_set = 'αβγδεζηθικλμνξoπρστυφχψω'
        elif numbering_preset == 'roman':
            char_set = []
            for entry in range(1, list_length):
                char_set.append(count_roman(self, entry))

    elif numbering_type == 'string':
        csv_object = csv.reader([self.options.source_string_list_string])
        char_set = list(csv_object)[0]

    elif numbering_type == 'csv':
        string_list = Inklin.csv_to_string_list(self, self.options.csv_filepath)
        if string_list == None:
            inkex.errormsg('No csv data found')
            sys.exit()
        ## Can we choose between csv row and csv first column ?
        elif self.options.csv_row_column_radio == 'row':
            row_index = self.options.csv_row_int
            char_set = string_list[row_index-1]
        elif self.options.csv_row_column_radio == 'column':
            column_list = []
            column_index = self.options.csv_column_int
            for row in string_list:
                column_list.append(row[column_index-1])
            char_set = column_list

        # Now lets trim the list according to the user range
        if self.options.csv_range_start_cb == 'true':
            range_start = self.options.csv_range_start_int - 1
        else:
            range_start = 0
        if self.options.csv_range_end_cb == 'true':
            range_end = self.options.csv_range_end_int
        else:
            range_end = None

        char_set = char_set[range_start:range_end]

    number_suffix = ''
    if self.options.page_numbering_offset_cb == 'true':
        number_offset = self.options.page_numbering_offset_int - 1
    else:
        number_offset = 0
    separator = self.options.source_label_separator

    # Lets add the suffix and prefix strings
    prefix_string = self.options.source_label_prefix
    suffix_string = self.options.source_label_suffix


    if self.options.page_numbering_zero_prefix_cb == 'true':
        zero_suffix = True
        suffix_offset = 0
    else:
        zero_suffix = False
        suffix_offset = len(char_set)

    for count in range(number_offset, list_length):

        if count // len(char_set) > 0 or zero_suffix:
            suffix_separator = separator
        else:
            suffix_separator = ''

        if count >= suffix_offset:
            number_suffix = count // len(char_set) + self.options.page_numbering_numerate_from_int

        char_set_index = count % len(char_set)

        label_list.append(f'{prefix_string}{char_set[char_set_index]}{suffix_separator}{number_suffix}{suffix_string}')

    return label_list


def simple_numbering(self):
    # Lets make a generic alignment dict
    alignment_dict = {}
    alignment_dict['v_align'] = self.options.simple_vertical_pos_radio
    alignment_dict['h_align'] = self.options.simple_horizontal_pos_radio
    # alignment_dict['h_io'] = self.options.simple_horizontal_inside_outside_radio
    alignment_dict['h_io'] = 'inside'
    # alignment_dict['v_io'] = self.options.simple_vertical_inside_outside_radio
    alignment_dict['v_io'] = 'inside'
    alignment_dict['perch'] = self.options.simple_perch_radio
    alignment_dict['bbox_type'] = 'geometric'

    # Lets make a generic alignment dict
    margin_dict = {}
    margin_dict['top'] = self.options.simple_top_margin
    margin_dict['left'] = self.options.simple_left_margin
    margin_dict['bottom'] = self.options.simple_bottom_margin
    margin_dict['right'] = self.options.simple_right_margin
    margin_dict['perch'] = self.options.simple_perch_radio
    # margin_dict['bbox_type'] = 'geometric'

    style_dict = {}
    style_dict['stroke'] = 'black'
    style_dict['stroke-width'] = 0
    style_dict['fill'] = 'yellow'
    # style_dict['fill-opacity'] = '0.2'

    chosen_units = self.options.simple_units_combo

    page_frame_layer = Layer()
    page_frame_layer_id = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    page_frame_layer.set('id', f'page_frame_layer_{page_frame_layer_id}')

    frame_cf = Inklin.conversions[chosen_units] / Inklin.conversions[self.svg.unit]

    page_frame_layer = frame_pages(self, self.svg, margin_dict, style_dict, page_frame_layer, frame_cf)

    frame_list = page_frame_layer.getchildren()

    # Decide if start page should be offset
    start_page = self.start_page - 1
    stop_page = self.stop_page
    frame_list = frame_list[start_page:stop_page]

    text_svg, string_list_bbox, label_group = create_labels(self, self.temp_folder, self.string_list, frame_list, self.font_dict, alignment_dict, self.svg)

    # page_frame_layer.delete()

    return string_list_bbox, page_frame_layer, label_group, alignment_dict, margin_dict

def object_numbering(self, selection_list):
    # Lets exit if nothing is selected
    if len(selection_list) < 1:
        return

    selection_list = [selection_list[0]]

    # Lets make a generic alignment dict
    alignment_dict = {}
    alignment_dict['v_align'] = self.options.object_vertical_pos_radio
    alignment_dict['h_align'] = self.options.object_horizontal_pos_radio
    alignment_dict['h_io'] = self.options.object_horizontal_inside_outside_radio
    alignment_dict['v_io'] = self.options.object_vertical_inside_outside_radio
    alignment_dict['perch'] = self.options.object_perch_radio
    alignment_dict['bbox_type'] = self.options.object_bbox_type_radio

    # inkex.errormsg(alignment_dict)

    # Create a watermark layer, can be deleted later

    page_translate_list_copy = deepcopy(self.page_translate_list)

    if self.options.object_treatment_radio == 'clone' or self.options.object_treatment_radio == 'remove':
        watermark_layer = paste_clones(self, selection_list, page_translate_list_copy)
    elif self.options.object_treatment_radio == 'duplicate':
        watermark_layer = paste_objects(self, selection_list, page_translate_list_copy)

    # Make copies of the selection list in a dummy group
    # selection_group = Group()
    # [selection_group.append(item.duplicate()) for item in selection_list]

    watermark_list = watermark_layer.getchildren()

    watermark_list.insert(0, selection_list[0])

    # Decide if start page should be offset
    start_page = self.start_page - 1
    stop_page = self.stop_page
    watermark_list = watermark_list[start_page:stop_page]

    text_svg, string_list_bbox, label_group = create_labels(self, self.temp_folder, self.string_list, watermark_list, self.font_dict, alignment_dict, self.svg)

    if self.options.object_treatment_radio == 'remove':
        for item in selection_list:
            item.delete()
            watermark_layer.delete()

    return text_svg, string_list_bbox, label_group


class PageNumbering(inkex.EffectExtension):

    class Settings:
        source_type = 'preset'
        source_preset = 'numbering'

    def add_arguments(self, pars):

        # Top Line Entries
        pars.add_argument("--format_combo", type=str, dest="format_combo", default='simple')

        # Main Notebook
        pars.add_argument("--page_numbering_notebook", type=str, dest="page_numbering_notebook", default=0)

        # Format Notebook
        pars.add_argument("--format_notebook", type=str, dest="format_notebook", default=0)
        # Simple Format Page
        pars.add_argument("--simple_vertical_pos_radio", type=str, dest="simple_vertical_pos_radio", default='top')
        pars.add_argument("--simple_horizontal_pos_radio", type=str, dest="simple_horizontal_pos_radio", default='left')
        pars.add_argument("--simple_perch_radio", type=str, dest="simple_perch_radio", default='baseline')
        pars.add_argument("--simple_top_margin", type=float, dest="simple_top_margin", default=1)
        pars.add_argument("--simple_left_margin", type=float, dest="simple_left_margin", default=1)
        pars.add_argument("--simple_bottom_margin", type=float, dest="simple_bottom_margin", default=1)
        pars.add_argument("--simple_right_margin", type=float, dest="simple_right_margin", default=1)
        pars.add_argument("--simple_units_combo", type=str, dest="simple_units_combo", default='px')

        pars.add_argument("--simple_lines_radio", type=str, dest="simple_lines_radio", default='simple')


        pars.add_argument("--simple_stroke_width_float", type=float, dest="simple_stroke_width_float", default=1)
        pars.add_argument("--simple_line_margin_float", type=float, dest="simple_line_margin_float", default=1)
        pars.add_argument("--simple_marker_margin_float", type=float, dest="simple_marker_margin_float", default=1)

        # Object Format Page
        pars.add_argument("--object_treatment_radio", type=str, dest="object_treatment_radio", default='remove')
        pars.add_argument("--watermark_treatment_radio", type=str, dest="watermark_treatment_radio", default='remove')
        pars.add_argument("--object_vertical_pos_radio", type=str, dest="object_vertical_pos_radio", default='top')
        pars.add_argument("--object_horizontal_pos_radio", type=str, dest="object_horizontal_pos_radio", default='left')
        pars.add_argument("--object_vertical_inside_outside_radio", type=str,
                          dest="object_vertical_inside_outside_radio", default='top')
        pars.add_argument("--object_horizontal_inside_outside_radio", type=str,
                          dest="object_horizontal_inside_outside_radio", default='left')
        pars.add_argument("--object_perch_radio", type=str, dest="object_perch_radio", default='baseline')
        pars.add_argument("--object_bbox_type_radio", type=str, dest="object_bbox_type_radio", default='baseline')
        # Font Page
        pars.add_argument("--font_size_float", type=float, dest="font_size_float", default=1)
        pars.add_argument("--font_units_combo", type=str, dest="font_units_combo", default='px')
        pars.add_argument("--font_fill_cp", type=str, dest="font_fill_cp")

        pars.add_argument("--bbox_type_radio", type=str, dest="bbox_type_radio", default='visual')

        pars.add_argument("--vertical_pos_radio", type=str, dest="vertical_pos_radio", default='middle')
        pars.add_argument("--horizontal_pos_radio", type=str, dest="horizontal_pos_radio", default='center')
        pars.add_argument("--horizontal_inside_outside_radio", type=str, dest="horizontal_inside_outside_radio",
                          default='center')
        pars.add_argument("--vertical_inside_outside_radio", type=str, dest="vertical_inside_outside_radio",
                          default='center')
        pars.add_argument("--perch_radio", type=str, dest="perch_radio", default='right')
        # Source Page
        pars.add_argument("--source_string_type", type=str, dest="source_string_type",
                          default='preset')
        pars.add_argument("--source_string_presets_combo", type=str, dest="source_string_presets_combo",
                          default='numbers')
        pars.add_argument("--page_numbering_start_cb", type=str, dest="page_numbering_start_cb", default='false')
        pars.add_argument("--page_numbering_start_int", type=int, dest="page_numbering_start_int", default=1)
        pars.add_argument("--page_numbering_stop_int", type=int, dest="page_numbering_stop_int", default=1)
        pars.add_argument("--page_numbering_stop_cb", type=str, dest="page_numbering_stop_cb", default='false')
        pars.add_argument("--page_numbering_offset_int", type=int, dest="page_numbering_offset_int", default=0)
        pars.add_argument("--page_numbering_offset_cb", type=str, dest="page_numbering_offset_cb", default='false')
        pars.add_argument("--page_numbering_numerate_from_int", type=int, dest="page_numbering_numerate_from_int", default=2)
        pars.add_argument("--source_string_list_string", type=str, dest="source_string_list_string", default='x')
        pars.add_argument("--source_repeat_type_radio", type=str, dest="source_repeat_type_radio", default='numerate')
        pars.add_argument("--source_label_prefix", type=str, dest="source_label_prefix", default='')
        pars.add_argument("--source_label_suffix", type=str, dest="source_label_suffix", default='')
        pars.add_argument("--source_label_separator", type=str, dest="source_label_separator", default='x')
        pars.add_argument("--page_numbering_zero_prefix_cb", type=str, dest="page_numbering_zero_prefix_cb", default='false')

        # CSV Page
        pars.add_argument("--csv_row_column_radio", type=str, dest="csv_row_column_radio", default='right')
        pars.add_argument("--csv_row_int", type=int, dest="csv_row_int", default=1)
        pars.add_argument("--csv_column_int", type=int, dest="csv_column_int", default=1)
        pars.add_argument("--csv_filepath", type=str, dest="csv_filepath", default=None)
        pars.add_argument("--csv_range_start_cb", type=str, dest="csv_range_start_cb", default='false')
        pars.add_argument("--csv_range_start_int", type=int, dest="csv_range_start_int", default=1)
        pars.add_argument("--csv_range_end_cb", type=str, dest="csv_range_end_cb", default='false')
        pars.add_argument("--csv_range_end_int", type=int, dest="csv_range_end_int", default=1)



    def effect(self):

        page_translate_list = get_page_translate(self)

        if page_translate_list is not None:
            number_of_pages = self.number_of_pages = len(page_translate_list)
        else:
            inkex.errormsg('Only 1 page found :(')
            return

        selection_list = self.svg.selected

        # numbering format
        self.source_preset = self.options.source_string_presets_combo
        self.source_type = self.options.source_string_type


        if self.options.page_numbering_start_cb == 'true':
            self.start_page = self.options.page_numbering_start_int
        else:
            self.start_page = 1
        if self.options.page_numbering_stop_cb == 'true':
            self.stop_page = self.options.page_numbering_stop_int
        else:
            self.stop_page = None

        string_list = create_char_set_number_list(self, number_of_pages+1, self.source_type, self.source_preset)

        # Create a temp folder for command calls
        temp_folder = Inklin.make_temp_folder(self)
        self.temp_folder = temp_folder

        # Get units
        found_units = self.svg.unit
        self.cf = Inklin.conversions[found_units]

        # Open Font Chooser config file, if not created yet use a safe fallback
        pango_font_dict = read_config_file(self, 'font_chooser.ini', 'FONT')
        if pango_font_dict is None:
            pango_font_dict = {'family': 'Sans', 'style': 'normal', 'variant': 'normal', 'weight': 'normal',
                               'size': '12288'}

        # Make font dict from Gtk3 FontChooser values (from .ini) and .inx values
        font_dict = {}
        font_dict['font_family'] = pango_font_dict['family']
        font_dict['font_size'] = self.options.font_size_float
        font_dict['font_units'] = self.options.font_units_combo
        font_dict['font_style'] = pango_font_dict['style']
        font_dict['font_weight'] = pango_font_dict['weight']
        font_dict['font_color'] = Inklin.rgb_long_to_svg_rgb(self, self.options.font_fill_cp)


        # page_translate_list = get_page_translate(self)

        self.string_list = string_list
        self.font_dict = font_dict
        self.page_translate_list = page_translate_list

        # For Simple format option:
        if self.options.format_combo == 'simple':

            lines_type = self.options.simple_lines_radio

            if lines_type == 'style' or lines_type == 'style_marker':
                if len(selection_list) < 1:
                    inkex.errormsg('Please select an object to style lines')
                    return

            if len(selection_list) > 0:
                line_style = selection_list[0].style
                marker_path, marker_url = get_marker(self, line_style)


            else:
                marker_path = marker_url = None

            # inkex.errormsg(f'{marker_path}  {marker_url}')

            string_list_bbox, page_frame_layer, label_group, alignment_dict, margin_dict = simple_numbering(self)

            if lines_type == 'style':
                marker_path = marker_url = None
                line_style_copy = line_style.copy()
                line_style_temp = line_style.copy()

                for item in line_style_temp:
                    if 'marker' in item:
                        del line_style_copy[item]

                create_label_lines(self, string_list_bbox, page_frame_layer, label_group, alignment_dict, margin_dict, marker_url, line_style_copy)
            elif lines_type == 'style_marker':
                line_style_copy = deepcopy(line_style)
                create_label_lines(self, string_list_bbox, page_frame_layer, label_group, alignment_dict, margin_dict, marker_url, line_style_copy)

            page_frame_layer.delete()
            if marker_path is not None:
                marker_path.delete()


        # For the Object format option:
        if self.options.format_combo == 'object':

            if self.page_translate_list is None:
                inkex.errormsg('Only one page found :(')
                return

            if len(selection_list) < 1:
                inkex.errormsg('Nothing Selected')
                return

            text_svg, string_list_bbox, label_group = object_numbering(self, selection_list)

            selection_watermark_list = []
            [selection_watermark_list.append(item) for item in selection_list]

            selection_watermark_list.pop(0)

            page_translate_list_copy = deepcopy(self.page_translate_list)
            watermark_treatment = self.options.watermark_treatment_radio
            if watermark_treatment == 'clone':
                watermark_layer2 = paste_clones(self, selection_watermark_list, page_translate_list_copy)
            elif watermark_treatment == 'duplicate':
                watermark_layer2 = paste_objects(self, selection_watermark_list, page_translate_list_copy)
            else:
                for item in selection_watermark_list:
                    item.delete()

            # Lets move the label group to the top
            self.svg.append(label_group)

        # Prevent id collisions on 2nd run
        try:
            for text_item in label_group:
                text_item.set_random_id()
        except:
            None

        # Cleanup temp folder
        if hasattr(self, 'inklin_temp_folder'):
            shutil.rmtree(self.inklin_temp_folder)


if __name__ == '__main__':
    PageNumbering().run()


